<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\external_entity\Contracts\ExternalEntityTypeInterface;
use Drupal\external_entity\Exception\NotFoundResourceDisplayException;
use Drupal\external_entity\Contracts\ExternalEntityConnectionInterface;
use Drupal\external_entity\Definition\ExternalEntityResourceDefinition;
use Drupal\external_entity\Contracts\ExternalEntityResourceDisplayInterface;

/**
 * Define the external entity type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "external_entity_type",
 *   label = @Translation("External Entity Type"),
 *   label_collection = @Translation("External Entity Types"),
 *   label_singular = @Translation("External Entity Type"),
 *   label_plural = @Translation("External Entity Types"),
 *   admin_permission = "administer external entity type",
 *   bundle_of = "external_entity",
 *   config_prefix = "type",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name"
 *   },
 *   config_export = {
 *     "type",
 *     "name",
 *     "storage"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\external_entity\Form\ExternalEntityTypeDefaultForm",
 *       "edit" = "\Drupal\external_entity\Form\ExternalEntityTypeDefaultForm",
 *       "delete" = "\Drupal\external_entity\Form\ExternalEntityDefaultDeleteForm",
 *       "default" = "\Drupal\external_entity\Form\ExternalEntityTypeDefaultForm",
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *     "list_builder" = "\Drupal\external_entity\Controller\EntityExternalTypeListBuilder"
 *   },
 *   links = {
 *     "collection" = "/admin/structure/external-entity/type",
 *     "add-form" = "/admin/structure/external-entity/type/add",
 *     "edit-form" = "/admin/structure/external-entity/manage/{external_entity_type}",
 *     "delete-form" = "/admin/structure/external-entity/manage/{external_entity_type}/delete",
 *   }
 * )
 */
class ExternalEntityType extends ConfigEntityBase implements ExternalEntityTypeInterface {

  /**
   * The external entity type.
   *
   * @var string
   */
  protected $type;

  /**
   * The external entity type name.
   *
   * @var string
   */
  protected $name;

  /**
   * An array of the storage info.
   *
   * @var array
   */
  protected $storage = [];

  /**
   * An array of cached resource options.
   *
   * @var array
   */
  protected $resourceOptions = [];

  /**
   * An array of cached resource properties.
   *
   * @var array
   */
  protected $resourceProperties = [];

  /**
   * An array of cached resource definitions.
   *
   * @var array array
   */
  protected $resourceDefinitions = [];

  /**
   * {@inheritDoc}
   */
  public function id(): ?string {
    return $this->type ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getStorage(): array {
    return $this->storage;
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceProperties(
    string $name,
    string $variation
  ): array {
    return $this->getAllResourceProperties()[$name][$variation] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function getAllResourceProperties(): array {
    if (empty($this->resourceProperties)) {
      foreach ($this->getResourceDefinitions() as $name => $resource) {
        foreach (array_keys($resource->getVariations()) as $variation) {
          $this->resourceProperties[$name][$variation] = $resource->getPropertiesByVariation(
            $variation
          );
        }
      }
    }

    return $this->resourceProperties;
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceDefinitions(): array {
    if (empty($this->resourceDefinitions)) {
      $this->resourceDefinitions = $this->getStorageConnection()
        ->connectionTypeInstance()
        ->fetchResourceDefinitions();
    }

    return $this->resourceDefinitions;
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceOptions(): array {
    $this->resourceOptions = [];

    if (!isset($this->resourceOptions) || empty($this->resourceOptions)) {
      foreach ($this->getResourceDefinitions() as $resource => $definition) {
        $this->resourceOptions['resources'][$resource] = $definition->getLabel();

        foreach ($definition->getVariations() as $variation => $info) {
          if (!isset($info['label'])) {
            continue;
          }
          $this->resourceOptions['variations'][$resource][$variation] = $info['label'];
          $properties = $definition->getPropertiesByVariation($variation);

          foreach ($properties as $property_name => $property_info) {
            $this->resourceOptions['properties'][$resource][$variation][$property_name] = $property_info['label'];
          }
        }
      }
    }

    return $this->resourceOptions;
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceAlterations(
    string $resource,
    string $variation
  ): array {
    $storage = $this->externalEntityResourceAlterStorage();
    return $storage->loadByProperties([
      'resource' => $resource,
      'variation' => $variation,
      'external_entity_type_id' => $this->id(),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getAllResourceAlterations(): array {
    $storage = $this->externalEntityResourceAlterStorage();
    return $storage->loadByProperties([
      'external_entity_type_id' => $this->id(),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getStorageConnectionId(): ?string {
    return $this->getStorage()['connection'] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getStorageConnection(): ExternalEntityConnectionInterface {
    return $this->externalEntityConnectionStorage()->load(
      $this->getStorageConnectionId()
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceVariations(
    string $resource
  ): array {
    return $this->getResourceDefinition($resource)->getVariations();
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceDisplay(
    string $resource,
    string $variation
  ): ?ExternalEntityResourceDisplayInterface {
    $display = $this->externalEntityResourceDisplayStorage()->load(
      "{$this->type}.{$resource}.{$variation}"
    );

    if (!isset($display)) {
      throw new NotFoundResourceDisplayException(
        $resource, $variation
      );
    }

    return $display;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTagsToInvalidate(): array {
    return [
      "connection:{$this->getStorageConnectionId()}"
    ];
  }

  /**
   * Get external entity definition.
   *
   * @param string $resource
   *   The external entity resource name.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntityResourceDefinition|null
   *    The external entity resource definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getResourceDefinition(
    string $resource
  ): ?ExternalEntityResourceDefinition {
    return $this->getResourceDefinitions()[$resource] ?? NULL;
  }

  /**
   * External entity resource display storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function externalEntityResourceDisplayStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage('external_entity_resource_display');
  }

  /**
   * External entity connection storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function externalEntityConnectionStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage('external_entity_connection');
  }

  /**
   * External entity resource alteration storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function externalEntityResourceAlterStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage('external_entity_resource_alter');
  }

}
