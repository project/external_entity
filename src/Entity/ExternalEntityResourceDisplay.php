<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\external_entity\Contracts\ExternalEntityRenderTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityResourceDisplayInterface;
use Drupal\external_entity\Contracts\ExternalEntityRenderTypeManagerInterface;

/**
 * Define the external entity resource display.
 *
 * @ConfigEntityType(
 *   id = "external_entity_resource_display",
 *   label = @Translation("External Entity Resource Display"),
 *   label_collection = @Translation("External Entity Resource Display"),
 *   label_singular = @Translation("External Entity Resource Display"),
 *   label_plural = @Translation("External Entity Resource Displays"),
 *   admin_permission = "administer external entity resource displays",
 *   config_prefix = "resource_display",
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   config_export = {
 *     "id",
 *     "render",
 *     "resource",
 *     "variation",
 *     "external_entity_type_id"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\external_entity\Form\ExternalEntityResourceDisplayForm",
 *       "edit" = "\Drupal\external_entity\Form\ExternalEntityResourceDisplayForm",
 *       "delete" = "\Drupal\external_entity\Form\ExternalEntityDefaultDeleteForm",
 *       "default" = "\Drupal\external_entity\Form\ExternalEntityResourceDisplayForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *     "list_builder" = "\Drupal\external_entity\Controller\ExternalEntityDisplayListBuilder",
 *   },
 *   links = {
 *     "collection" = "/admin/structure/external-entity/manage/{external_entity_type}/displays",
 *     "add-form" = "/admin/structure/external-entity/manage/{external_entity_type}/displays/add",
 *     "edit-form" = "/admin/structure/external-entity/display/{external_entity_resource_display}/edit",
 *     "delete-form" = "/admin/structure/external-entity/display/{external_entity_resource_display}/delete",
 *   }
 * )
 */
class ExternalEntityResourceDisplay extends ExternalEntityTypeConfigEntityBase implements ExternalEntityResourceDisplayInterface {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $resource;

  /**
   * @var string
   */
  protected $variation;

  /**
   * @var array
   */
  protected $render = [];

  /**
   * {@inheritDoc}
   */
  public function id(): ?string {
    if (!isset($this->id) || $this->id !== $this->currentId()) {
      $this->id = $this->currentId();
    }

    return $this->id;
  }

  /**
   * {@inheritDoc}
   */
  public function label(): ?TranslatableMarkup {
    return $this->t(
      'Resource @name with @variation display',
      [
        '@name' => $this->getResource(),
        '@variation' => $this->getVariation(),
      ]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getRender(): array {
    return $this->render;
  }

  /**
   * {@inheritDoc}
   */
  public function getResource(): ?string {
    return $this->resource;
  }

  /**
   * {@inheritDoc}
   */
  public function getVariation(): ?string {
    return $this->variation;
  }

  /**
   * {@inheritDoc}
   */
  public function getRenderType(): ?string {
    return $this->getRender()['type'] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getRenderSettings(): ?array {
    return $this->getRender()['settings'] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function createRenderInstance(): ?ExternalEntityRenderTypeInterface {
    $render_type_id = $this->getRenderType();
    $render_manager = $this->externalEntityRenderTypeManager();

    if (!$render_manager->hasDefinition($render_type_id)) {
      return NULL;
    }

    $instance = $render_manager->createInstance(
      $render_type_id,
      $this->getRenderSettings()
    );

    if ($instance instanceof ExternalEntityRenderTypeInterface) {
      $instance->setExternalEntityDisplay($this);
    }

    return $instance;
  }

  /**
   * Determine if the external entity resource display exists.
   *
   * @return bool
   *   Return TRUE if entity already exist; otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists(): bool {
    return (bool) $this->getEntityTypeStorage()->getQuery()
      ->count()
      ->condition('resource', $this->getResource())
      ->condition('variation', $this->getVariation())
      ->condition('external_entity_type_id', $this->getExternalEntityTypeId())
      ->accessCheck()
      ->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTagsToInvalidate(): array {
    $render = $this->createRenderInstance();

    return Cache::mergeTags(
      $render->getCacheTags(),
      parent::getCacheTagsToInvalidate()
    );
  }

  /**
   * External entity resource display current ID.
   *
   * @return string
   *   The current external resource display ID.
   */
  protected function currentId(): string {
    return implode('.', [
      $this->getExternalEntityTypeId(),
      $this->getResource(),
      $this->getVariation(),
    ]);
  }

  /**
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityTypeStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage($this->getEntityTypeId());
  }

  /**
   * External entity render type manager.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityRenderTypeManagerInterface
   */
  protected function externalEntityRenderTypeManager(): ExternalEntityRenderTypeManagerInterface {
    return \Drupal::service('plugin.manager.external_entity.render_type');
  }

}
