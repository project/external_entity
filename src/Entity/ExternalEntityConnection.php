<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\external_entity\Contracts\ExternalEntityConnectionAwareInterface;
use Drupal\external_entity\Contracts\ExternalEntityConnectionInterface;
use Drupal\external_entity\Contracts\ExternalEntityConnectionTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityConnectionTypeManagerInterface;
use Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeManagerInterface;

/**
 * Define the external entity connection.
 *
 * @ConfigEntityType(
 *   id = "external_entity_connection",
 *   label = @Translation("External Entity Connection"),
 *   label_plural = @Translation("External Entity Connection"),
 *   label_singular = @Translation("External Entity Connection"),
 *   label_collection = @Translation("External Entity Connection"),
 *   admin_permission = "administer external entity connection",
 *   config_prefix = "connection",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "connection_type",
 *     "connection_settings",
 *     "authentication_type",
 *     "authentication_settings"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\external_entity\Form\ExternalEntityConnectionForm",
 *       "edit" = "\Drupal\external_entity\Form\ExternalEntityConnectionForm",
 *       "delete" = "\Drupal\external_entity\Form\ExternalEntityDefaultDeleteForm",
 *       "default" = "\Drupal\external_entity\Form\ExternalEntityConnectionForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *     "list_builder" = "\Drupal\external_entity\Controller\ExternalEntityConnectionListBuilder"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/external-entity/connection",
 *     "add-form" = "/admin/config/services/external-entity/connection/add",
 *     "edit-form" = "/admin/config/services/external-entity/connection/{external_entity_connection}",
 *     "delete-form" = "/admin/config/services/external-entity/connection/{external_entity_connection}/delete"
 *   }
 * )
 */
class ExternalEntityConnection extends ConfigEntityBase implements ExternalEntityConnectionInterface {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var string
   */
  protected $connection_type;

  /**
   * @var string
   */
  protected $authentication_type;

  /**
   * @var array
   */
  protected $connection_settings = [];

  /**
   * @var array
   */
  protected $authentication_settings = [];

  /**
   * {@inheritDoc}
   */
  public function connectionTypeId(): ?string {
    return $this->connection_type;
  }

  /**
   * {@inheritDoc}
   */
  public function connectionSettings(): array {
    return $this->connection_settings;
  }

  /**
   * {@inheritDoc}
   */
  public function authenticationTypeId(): ?string {
    return $this->authentication_type;
  }

  /**
   * {@inheritDoc}
   */
  public function authenticationSettings(): array {
    return $this->authentication_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function exists(string $id): bool {
    return (bool) $this->getQuery()->condition('id', $id)->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function getOriginal(): ?ExternalEntityConnectionInterface {
    $original = NULL;

    try {
      $original = $this->getStorage()->load(
        $this->id()
      );
    }
    catch (\Exception $exception) {
      watchdog_exception('external_entity', $exception);
    }

    return $original;
  }

  /**
   * {@inheritDoc}
   */
  public function hasConnectionType(): bool {
    $connection_type_id = $this->connectionTypeId();

    return isset($connection_type_id)
      && $this->connectionTypeManager()->hasDefinition($connection_type_id);
  }

  /**
   * {@inheritDoc}
   */
  public function connectionTypeInstance(): ?ExternalEntityConnectionTypeInterface {
    $instance = NULL;

    try {
      if (!$this->hasConnectionType()) {
        return NULL;
      }

      $instance = $this->connectionTypeManager()->createInstance(
        $this->connectionTypeId(),
        $this->connectionSettings()
      );

      if ($instance instanceof ExternalEntityConnectionAwareInterface) {
        $instance->setConnection($this);
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('external_entity', $exception);
    }

    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function hasAuthenticationType(): bool {
    $authentication_type_id = $this->authenticationTypeId();

    return isset($authentication_type_id)
      && $this->authenticationTypeManage()->hasDefinition($authentication_type_id);
  }

  /**
   * {@inheritDoc}
   */
  public function createAuthenticationTypeInstance(): ?ExternalEntityAuthenticationTypeInterface {
    $instance = NULL;

    try {
      if (!$this->hasAuthenticationType()) {
        return NULL;
      }

      $instance = $this->authenticationTypeManage()->createInstance(
        $this->authenticationTypeId(),
        $this->authenticationSettings()
      );

      if ($instance instanceof ExternalEntityConnectionAwareInterface) {
        $instance->setConnection($this);
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('external_entity', $exception);
    }

    return $instance;
  }

  /**
   * Get entity storage query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The entity storage query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getQuery(): QueryInterface {
    return $this->getStorage()->getQuery();
  }

  /**
   * Get entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage interface.
   */
  protected function getStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage($this->getEntityTypeId());
  }

  /**
   * Get the connection type manager.
   */
  protected function connectionTypeManager(): ExternalEntityConnectionTypeManagerInterface {
    return \Drupal::service('plugin.manager.external_entity.connection_type');
  }

  /**
   * Get the authentication type manager.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeManagerInterface
   *   The authentication type manager instance.
   */
  protected function authenticationTypeManage(): ExternalEntityAuthenticationTypeManagerInterface {
    return \Drupal::service('plugin.manager.external_entity.authentication_type');
  }

}
