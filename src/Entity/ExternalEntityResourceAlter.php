<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\external_entity\Definition\ExternalEntityResourceDefinition;

/**
 * Define the external entity resource alteration.
 *
 * @ConfigEntityType(
 *   id = "external_entity_resource_alter",
 *   label = @Translation("External Entity Resource Alteration"),
 *   label_collection = @Translation("External Entity Resources"),
 *   admin_permission = "administer external entity resource alterations",
 *   config_prefix = "resource_alteration",
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   config_export = {
 *     "id",
 *     "resource",
 *     "property",
 *     "variation",
 *     "alterations",
 *     "external_entity_type_id"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\external_entity\Form\ExternalEntityResourceAlterForm",
 *       "edit" = "\Drupal\external_entity\Form\ExternalEntityResourceAlterForm",
 *       "delete" = "\Drupal\external_entity\Form\ExternalEntityDefaultDeleteForm",
 *       "default" = "\Drupal\external_entity\Form\ExternalEntityResourceAlterForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\external_entity\Entity\Routing\ExternalEntityResourceAlterRouteProvider"
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/structure/external-entity/manage/{external_entity_type}/resources",
 *     "add-form" = "/admin/structure/external-entity/manage/{external_entity_type}/resources/alteration/{resource}/{variation}/{property}",
 *     "edit-form" = "/admin/structure/external-entity/resources/alteration/{external_entity_resource_alter}/edit",
 *     "delete-form" = "/admin/structure/external-entity/resources/alteration/{external_entity_resource_alter}/delete",
 *   }
 * )
 */
class ExternalEntityResourceAlter extends ExternalEntityTypeConfigEntityBase {

  use StringTranslationTrait;

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $resource;

  /**
   * @var string
   */
  protected $property;

  /**
   * @var string
   */
  protected $variation;

  /**
   * @var array
   */
  protected $alterations = [];

  /**
   * {@inheritDoc}
   */
  public function id(): ?string {
    if (!isset($this->id)) {
      $resource = $this->getResource();
      $property = $this->getProperty();
      $variation = $this->getVariation();
      $entity_type_id = $this->getExternalEntityTypeId();

      if (isset($entity_type_id, $resource, $variation, $property)) {
        $this->id = implode('.', [
          $entity_type_id,
          $resource,
          $variation,
          $property
        ]);
      }
    }

    return $this->id;
  }

  /**
   * {@inheritDoc}
   */
  public function label(): ?TranslatableMarkup {
    return $this->t(
      'Resource @name with @variation/@property alteration',
      [
        '@name' => $this->getResource(),
        '@property' => $this->getProperty(),
        '@variation' => $this->getVariation(),
      ]
    );
  }

  /**
   * Get the external entity resource.
   *
   * @return string|NULL
   *   The external entity resource.
   */
  public function getResource(): ?string {
    return $this->resource;
  }

  /**
   * Set the external entity resource.
   *
   * @param string $resource
   *   The external entity resource.
   *
   * @return \Drupal\external_entity\Entity\ExternalEntityResourceAlter
   */
  public function setResource(string $resource): self {
    $this->resource = $resource;
    return $this;
  }

  /**
   * Get the external entity resource property.
   *
   * @return string|NULL
   *   The resource property name.
   */
  public function getProperty(): ?string {
    return $this->property;
  }

  /**
   * Set the external entity resource property.
   *
   * @param string $property
   *   The resource property name.
   *
   * @return \Drupal\external_entity\Entity\ExternalEntityResourceAlter
   */
  public function setProperty(string $property): self {
    $this->property = $property;
    return $this;
  }

  /**
   * Get the external entity resource variation.
   *
   * @return string|NULL
   */
  public function getVariation(): ?string {
    return $this->variation;
  }

  /**
   * Set the external entity resource variation.
   *
   * @param string $variation
   *   The resource variation.
   */
  public function setVariation(string $variation): self {
    $this->variation = $variation;
    return $this;
  }

  /**
   * Get the external entity resource alterations.
   *
   * @return array
   *   An array of resource alterations.
   */
  public function getAlterations(): array {
    return array_filter($this->alterations);
  }

  /**
   * {@inheritDoc}
   */
  public function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'add-form') {
      $uri_route_parameters['resource'] = $this->getResource();
      $uri_route_parameters['property'] = $this->getProperty();
      $uri_route_parameters['variation'] = $this->getVariation();
    }

    return $uri_route_parameters;
  }

  /**
   * Get the current resource definition.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntityResourceDefinition
   *   The external entity resource definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResourceDefinition(): ?ExternalEntityResourceDefinition {
    return $this->externalEntityType()
      ->getResourceDefinitions()[$this->getResource()] ?? NULL;
  }

  /**
   * Get the current resource property information.
   *
   * @return array
   *   An array of resource property information.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResourcePropertyInfo(): array {
    return $this->getResourceDefinition()
        ->getPropertiesByVariation($this->getVariation())[$this->getProperty()] ?? [];
  }

}
