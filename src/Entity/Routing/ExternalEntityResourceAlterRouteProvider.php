<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Routing;

use Symfony\Component\Routing\Route;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;

/**
 * Define the external entity resource alter route provider.
 */
class ExternalEntityResourceAlterRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritDoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('collection')) {
      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $label */
      $label = $entity_type->getCollectionLabel();

      $route = new Route($entity_type->getLinkTemplate('collection'));
      $route
        ->addDefaults([
          '_title' => $label->getUntranslatedString(),
          '_title_arguments' => $label->getArguments(),
          '_title_context' => $label->getOption('context'),
          '_controller' => '\Drupal\external_entity\Controller\ExternalEntityTypeResourceList::build',
        ])
        ->setRequirement('_permission', 'access all external entity type resources');

      return $route;
    }
  }

}
