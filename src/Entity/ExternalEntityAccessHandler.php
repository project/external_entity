<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Define the external entity access control handler.
 */
class ExternalEntityAccessHandler extends EntityAccessControlHandler {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $operation
   * @param \Drupal\Core\Session\AccountInterface|null $account
   * @param false $return_as_object
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface|null
   */
  public function access(
    EntityInterface $entity,
    $operation,
    AccountInterface $account = NULL,
    $return_as_object = FALSE
  ) {
    $result = parent::access($entity, $operation, $account, TRUE)
      ->cachePerPermissions();

    if ($operation === 'view') {
      $result = AccessResult::allowed();
    }

    return $return_as_object ? $result : $result->isAllowed();
  }

}
