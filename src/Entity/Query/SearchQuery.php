<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query;

/**
 * Define the search query object.
 */
class SearchQuery {

  /**
   * @var array
   */
  protected $sort = [];

  /**
   * @var array
   */
  protected $range = [];

  /**
   * @var array
   */
  protected $pager = [];

  /**
   * @var array
   */
  protected $filter = [];

  /**
   * Set the search query filter.
   *
   * @param string $property
   *   The filter property name.
   * @param mixed $value
   *   The filter property value.
   * @param string $operator
   *   The filter operator.
   *
   * @return $this
   */
  public function setFilter(
    string $property,
    $value,
    string $operator = '='
  ): self {
    $this->filter[$property]['value'] = $value;
    $this->filter[$property]['operator'] = $operator;

    return $this;
  }

  /**
   * Set the search query range.
   *
   * @param int $start
   *   The query range start.
   * @param int $length
   *   The query range length.
   *
   * @return $this
   */
  public function setRange(
    int $start = 0,
    int $length = 0
  ): self {
    $this->range = [
      'start' => $start,
      'length' => $length,
    ];

    return $this;
  }

  /**
   * Set the search query sort.
   *
   * @param string $source
   *   The sort source.
   * @param string $direction
   *   The sort direction.
   *
   * @return $this
   */
  public function setSort(
    string $source,
    string $direction = 'ASC'
  ): self {
    $this->sort[] = [
      'source' => $source,
      'direction' => $direction,
    ];

    return $this;
  }

  /**
   * Set the search query pager limit.
   *
   * @param int $limit
   *   The pager limit integer.
   *
   * @return \Drupal\external_entity\Entity\Query\SearchQuery
   */
  public function setPagerLimit(int $limit): self {
    $this->pager['limit'] = $limit;

    return $this;
  }

  /**
   * Format the search query.
   *
   * @return array[]
   *   An array of the search query.
   */
  public function format(): array {
    return array_filter([
      'sort' => $this->sort,
      'range' => $this->range,
      'pager' => $this->pager,
      'filter' => $this->filter,
    ]);
  }

}
