<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query\Connection;

use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\external_entity\Entity\Query\QueryFactoryInterface;

/**
 * Define the connection query factory service.
 */
class QueryFactory implements QueryFactoryInterface {

  /**
   * @var array
   */
  protected $namespaces = [];

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The connection query factory constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->namespaces = QueryBase::getNamespaces($this);
  }

  /**
   * {@inheritDoc}
   */
  public function get(
    string $resource,
    EntityTypeInterface $entity_type,
    string $entity_type_id,
    string $conjunction
  ): QueryInterface {
    $class = QueryBase::getClass($this->namespaces, 'Query');

    return new $class(
      $resource,
      $entity_type,
      $entity_type_id,
      $conjunction,
      $this->namespaces,
      $this->entityTypeManager
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getAggregate(
    string $resource,
    EntityTypeInterface $entity_type,
    string $entity_type_id,
    string $conjunction
  ): QueryAggregateInterface {
    $class = QueryBase::getClass($this->namespaces, 'QueryAggregate');

    return new $class(
      $resource,
      $entity_type,
      $entity_type_id,
      $conjunction,
      $this->namespaces,
      $this->entityTypeManager
    );
  }

}
