<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query\Connection;

use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\external_entity\Entity\Query\SearchQuery;
use Drupal\external_entity\Contracts\ExternalEntityTypeInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\external_entity\Definition\ExternalEntitySearchDefinition;

/**
 * Define the entity connection query class.
 */
class Query extends QueryBase implements QueryInterface {

  use DependencySerializationTrait;

  /**
   * @var string
   */
  protected $resource;

  /**
   * @var \Drupal\external_entity\Entity\Query\SearchQuery
   */
  protected $query;

  /**
   * @var string
   */
  protected $entityTypeId;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Construct for the connection query object.
   *
   * @pagram string $resource
   *   The query resource.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param string $conjunction
   *   - AND: all of the conditions on the query need to match.
   *   - OR: at least one of the conditions on the query need to match.
   * @param array $namespaces
   *   List of potential namespaces of the classes belonging to this query.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    string $resource,
    EntityTypeInterface $entity_type,
    string $entity_type_id,
    string $conjunction,
    array $namespaces,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($entity_type, $conjunction, $namespaces);

    $this->resource = $resource;
    $this->entityTypeId = $entity_type_id;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function execute() {
    return $this
      ->prepare()
      ->compile()
      ->results();
  }

  /**
   * Prepare the query object.
   *
   * @return $this
   */
  protected function prepare(): self {
    $this->query = new SearchQuery();
    return $this;
  }

  /**
   * Compile the query.
   *
   * @return $this
   */
  protected function compile(): self {
    $query = $this->query;
    $this->condition->compile($query);

    foreach ($this->sort as $sort) {
      if (!isset($sort['field'])) {
        continue;
      }
      $query->setSort(
        $sort['field'],
        $sort['direction']
      );
    }

    if ($limit = $this->pager['limit'] ?? NULL) {
      $query->setPagerLimit($limit);
    }

    if (isset($this->range) && !empty($this->range)) {
      $query->setRange(
        $this->range['start'],
        $this->range['length']
      );
    }

    return $this;
  }

  /**
   * Format the query as a string.
   *
   * @return string
   */
  public function __toString() {
    $query = urldecode(
      http_build_query($this->query->format())
    );
    return "https://example.com?{$query}";
  }

  /**
   * Return the results for the query.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntitySearchDefinition
   *   An array of result items.
   */
  protected function results(): ?ExternalEntitySearchDefinition {
    try {
      $instance = $this->bundleEntityType()
        ->getStorageConnection()
        ->connectionTypeInstance();

      return $instance->searchDefinitions(
        $this->resource,
        $this->query
      );
    } catch (\Exception $exception) {
      watchdog_exception('external_entity', $exception);
    }

    return [];
  }

  /**
   * Get bundle entity type instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityTypeInterface
   *   The bundle entity type instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function bundleEntityType(): ExternalEntityTypeInterface {
    return $this->bundleEntityTypeStorage()->load($this->entityTypeId);
  }

  /**
   * Get bundle entity type storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The bundle entity type storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function bundleEntityTypeStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage(
      $this->entityType->getBundleEntityType()
    );
  }

}
