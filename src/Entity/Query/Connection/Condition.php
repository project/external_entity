<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query\Connection;

use Drupal\Core\Entity\Query\ConditionBase;

/**
 * Define the connection query condition.
 */
class Condition extends ConditionBase {

  /**
   * {@inheritDoc}
   */
  public function exists($field, $langcode = NULL) {
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function notExists($field, $langcode = NULL) {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function compile($query): self {
    /** @var \Drupal\external_entity\Entity\Query\SearchQuery $query */

    foreach ($this->conditions() as $condition) {
      if (!isset($condition['field'], $condition['value'])) {
        continue;
      }

      $query->setFilter(
        $condition['field'],
        $condition['value'],
        $condition['operator'] ?? '=',
      );
    }

    return $this;
  }

}
