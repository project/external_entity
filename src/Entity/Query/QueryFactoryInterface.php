<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;

/**
 * Define the connection query factory interface.
 */
interface QueryFactoryInterface {

  /**
   * Instantiates an entity query for a given entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param string $entity_type_id
   *   The external entity type ID.
   * @param string $conjunction
   *   The operator to use to combine conditions: 'AND' or 'OR'.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   An entity query for a specific configuration entity type.
   */
  public function get(
    string $resource,
    EntityTypeInterface $entity_type,
    string $entity_type_id,
    string $conjunction
  ): QueryInterface;

  /**
   * Instantiates an aggregation query object for a given entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param string $entity_type_id
   *   The external entity type ID.
   * @param string $conjunction
   *   - AND: all of the conditions on the query need to match.
   *   - OR: at least one of the conditions on the query need to match.
   *
   * @return \Drupal\Core\Entity\Query\QueryAggregateInterface
   *   The query object that can query the given entity type.
   *
   * @throws \Drupal\Core\Entity\Query\QueryException
   */
  public function getAggregate(
    string $resource,
    EntityTypeInterface $entity_type,
    string $entity_type_id,
    string $conjunction
  ): QueryAggregateInterface;

}
