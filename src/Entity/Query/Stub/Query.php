<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query\Stub;

use Drupal\Core\Entity\Query\QueryBase;

/**
 * Define the external entity query stub.
 *
 * This only exists to ensure that the Drupal framework can operate
 * without throwing errors, due to the query object not existing for external
 * entity types as we need additional parameters to load the external resource
 * prior to receiving the query results.
 *
 * @see \Drupal\external_entity\Entity\ExternalEntityStorage::getResourceQuery
 */
class Query extends QueryBase {

  /**
   * {@inheritDoc}
   */
  public function execute() {
    return [];
  }
}
