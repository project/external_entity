<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query\Stub;

use Drupal\Core\Entity\Query\ConditionBase;

/**
 * Define the external entity query stub condition object.
 */
class Condition extends ConditionBase {

  /**
   * {@inheritDoc}
   */
  public function exists($field, $langcode = NULL) {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function notExists($field, $langcode = NULL) {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function compile($query) {
    return $this;
  }
}
