<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity\Query\Stub;

use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\Query\QueryFactoryInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;

/**
 * Define the external entity query factory for the stub implementation.
 */
class QueryFactory implements QueryFactoryInterface {

  /**
   * @var array
   */
  protected array $namespaces = [];

  /**
   * The query factory constructor.
   */
  public function __construct() {
    $this->namespaces = QueryBase::getNamespaces($this);
  }

  /**
   * {@inheritDoc}
   */
  public function get(
    EntityTypeInterface $entity_type,
    $conjunction
  ): QueryInterface {
    $class = QueryBase::getClass($this->namespaces, 'Query');

    return new $class(
      $entity_type,
      $conjunction,
      $this->namespaces,
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getAggregate(
    EntityTypeInterface $entity_type,
    $conjunction
  ): QueryAggregateInterface {
    $class = QueryBase::getClass($this->namespaces, 'QueryAggregate');

    return new $class(
      $entity_type,
      $conjunction,
      $this->namespaces,
    );
  }
}
