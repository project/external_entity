<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\external_entity\Contracts\ExternalEntityTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityTypeConfigEntityInterface;

/**
 * Define the external entity type configuration entity base.
 *
 * Extend this class when you need to relate the configuration to an external
 * entity type. You will need to update the parent annotation to include
 * external_entity_type_id in the config_export, so that it can be lookup. In the
 * links definitions you'll be able to use {external_entity_type}.
 */
abstract class ExternalEntityTypeConfigEntityBase extends ConfigEntityBase implements ExternalEntityTypeConfigEntityInterface {

  use StringTranslationTrait;

  /**
   * @var string
   */
  protected $external_entity_type_id;

  /**
   * {@inheritDoc}
   */
  public function getExternalEntityTypeId(): ?string {
    return $this->external_entity_type_id;
  }

  /**
   * {@inheritDoc}
   */
  public function setExternalEntityTypeId(
    string $external_entity_type_id
  ): ExternalEntityTypeConfigEntityInterface {
    $this->external_entity_type_id = $external_entity_type_id;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if (in_array($rel, ['collection', 'add-form'], TRUE)) {
      $uri_route_parameters['external_entity_type'] = $this->getExternalEntityTypeId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritDoc}
   */
  public function externalEntityType(): ?ExternalEntityTypeInterface {
    if ($external_entity_type_id = $this->getExternalEntityTypeId()) {
      return $this->externalEntityTypeStorage()->load(
        $external_entity_type_id
      );
    }

    return NULL;
  }

  /**
   * External entity type storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function externalEntityTypeStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage('external_entity_type');
  }

}
