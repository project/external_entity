<?php

declare(strict_types=1);

namespace Drupal\external_entity\Entity;

use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\SynchronizableEntityTrait;

/**
 * Define the external entity base.
 */
abstract class ExternalEntityBase extends EntityBase implements \IteratorAggregate, EntityInterface, ContentEntityInterface {

  use SynchronizableEntityTrait;

  /**
   * {@inheritDoc}
   */
  public static function bundleFieldDefinitions(
    EntityTypeInterface $entity_type,
    $bundle,
    array $base_field_definitions
  ): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->getFields());
  }

  /**
   * {@inheritDoc}
   */
  public function getTranslatableFields($include_computed = TRUE): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function onChange($field_name): void {
  }

  /**
   * {@inheritDoc}
   */
  public function validate(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function isValidationRequired(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function setValidationRequired($required): self {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function isNewRevision(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function setNewRevision($value = TRUE): void {
  }

  /**
   * {@inheritDoc}
   */
  public function getRevisionId(): ?int {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getLoadedRevisionId(): ?int {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function updateLoadedRevisionId(): self {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function isDefaultRevision($new_value = NULL): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function wasDefaultRevision(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function isLatestRevision(): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function preSaveRevision(
    EntityStorageInterface $storage,
    \stdClass $record
  ): void {
  }

  /**
   * {@inheritDoc}
   */
  public function isDefaultTranslation(): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function isNewTranslation(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getTranslationLanguages($include_default = TRUE): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getTranslation($langcode): self {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getUntranslated(): self {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function addTranslation($langcode, array $values = []): self {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function isTranslatable(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function hasTranslationChanges(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function hasTranslation($langcode): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function removeTranslation($langcode): void {}

  /**
   * {@inheritDoc}
   */
  public function isLatestTranslationAffectedRevision(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function setRevisionTranslationAffected($affected): self {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function isRevisionTranslationAffected(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function isRevisionTranslationAffectedEnforced(): bool {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function setRevisionTranslationAffectedEnforced($enforced): self {
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function isDefaultTranslationAffectedOnly(): bool {
    return FALSE;
  }

}
