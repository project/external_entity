<?php

declare(strict_types=1);

namespace Drupal\external_entity\Definition;

/**
 * Define a simple definition base class.
 */
abstract class SimpleDefinitionBase {

  /**
   * Simple definition constructor.
   *
   * @param array $values
   *   An array of the properties and values.
   */
  public function __construct(array $values) {
    foreach ($values as $key => $value) {
      if (!property_exists($this, $key)) {
        continue;
      }
      $this->{$key} = $this->structureValue(
        $key,
        $value
      );
    }
  }

  /**
   * Output the object properties.
   *
   * @return array
   *   An array of the definition properties.
   */
  public function toArray(): array {
    return get_object_vars($this);
  }

  /**
   * Structure the definition value.
   *
   * @param string $key
   *   The structured key.
   * @param mixed $value
   *   The structured value.
   *
   * @return mixed
   *   The structured value.
   */
  protected function structureValue(
    string $key,
    $value
  ) {
    return $value;
  }

}
