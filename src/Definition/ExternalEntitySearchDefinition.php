<?php

declare(strict_types=1);

namespace Drupal\external_entity\Definition;


/**
 * Define the external entity search definition.
 */
class ExternalEntitySearchDefinition extends SimpleDefinitionBase {

  /**
   * @var array
   */
  public $info = [];

  /**
   * @var array
   */
  public $results = [];

  /**
   * @return array
   */
  public function getInfo(): array {
    return $this->info;
  }

  /**
   * @param array $info
   */
  public function setInfo(array $info): void {
    $this->info = $info;
  }

  /**
   * @return \Drupal\external_entity\Definition\ExternalEntityDefaultDefinition[]
   */
  public function getResults(): array {
    return $this->results;
  }

  /**
   * @param array $results
   *
   * @return \Drupal\external_entity\Definition\ExternalEntitySearchDefinition
   */
  public function setResults(array $results): self {
    $this->results = $results;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function structureValue(string $key, $value) {
    if ($key == 'results' && is_array($value)) {
      foreach ($value as &$alter_value) {
        $alter_value = new ExternalEntityDefaultDefinition(
          $alter_value
        );
      }
    }

    return $value;
  }

}
