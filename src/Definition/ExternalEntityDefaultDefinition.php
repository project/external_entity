<?php

declare(strict_types=1);

namespace Drupal\external_entity\Definition;

/**
 * Define external entity default definition.
 */
class ExternalEntityDefaultDefinition extends SimpleDefinitionBase {

  /**
   * @var string
   */
  public $path;

  /**
   * @var string
   */
  public $variation;

  /**
   * @var string
   */
  public $resource;

  /**
   * @var array
   */
  public $properties = [];

  /**
   * Get the definition ID value.
   *
   * @return int|null
   */
  public function id(): ?int {
    return $this->getFirstValue('id');
  }

  /**
   * Get the definition label value.
   *
   * @return string|null
   */
  public function label(): ?string {
    return $this->getFirstValue('label');
  }

  /**
   * Get the definition UUID value.
   *
   * @return string|null
   */
  public function uuid(): ?string {
    return $this->getFirstValue('uuid');
  }

  /**
   * Get the definition path.
   *
   * @return string|null
   *   The external entity path.
   */
  public function getPath(): ?string {
    return $this->path;
  }

  /**
   * Set the definition path.
   *
   * @param string $path
   *   The external entity path.
   */
  public function setPath(string $path): self {
    $this->path = $path;

    return $this;
  }

  /**
   * Get the definition properties.
   *
   * @return array
   *   An array of properties.
   */
  public function getProperties(): array {
    return $this->properties;
  }

  /**
   * Set the definition properties.
   *
   * @param array $properties
   *   An array of properties.
   */
  public function setProperties(array $properties): self {
    $this->properties = $properties;

    return $this;
  }

  /**
   * Get the definition resource.
   *
   * @return string
   *   The definition resource name.
   */
  public function getResource(): string {
    return $this->resource;
  }

  /**
   * Set the definition resource.
   *
   * @param string $resource
   *   The definition resource name.
   */
  public function setResource(string $resource): self {
    $this->resource = $resource;

    return $this;
  }

  /**
   * Get the definition variation.
   *
   * @return string
   *   The definition variation.
   */
  public function getVariation(): string {
    return $this->variation;
  }

  /**
   * Set the definition variation.
   *
   * @param string $variation
   *   The definition variation.
   */
  public function setVariation(string $variation): void {
    $this->variation = $variation;
  }

  /**
   * Get the first property value.
   *
   * @param string $name
   *   The name of property
   *
   * @return string|null
   *   The first property value.
   */
  protected function getFirstValue(string $name): ?string {
    return $this->properties[$name]['value'][0]['value'] ?? NULL;
  }

}
