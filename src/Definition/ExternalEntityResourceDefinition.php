<?php

declare(strict_types=1);

namespace Drupal\external_entity\Definition;

/**
 * Define the external entity resource definition.
 */
class ExternalEntityResourceDefinition extends SimpleDefinitionBase {

  /**
   * @var string
   */
  public $label;

  /**
   * @var string
   */
  public $type;

  /**
   * @var bool
   */
  public $status;

  /**
   * @var array
   */
  public $properties = [];

  /**
   * @var array
   */
  public $variations = [];

  /**
   * @return string
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * Set resource label.
   *
   * @param string $label
   *   The resource label.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntityResourceDefinition
   */
  public function setLabel(string $label): self {
    $this->label = $label;

    return $this;
  }

  /**
   * Get resource status.
   *
   * @return bool
   */
  public function getStatus(): bool {
    return $this->status;
  }

  /**
   * Set resource status.
   *
   * @param bool $status
   *   The status value.
   */
  public function setStatus(bool $status): self {
    $this->status = $status;

    return $this;
  }

  /**
   * @return array
   */
  public function getVariations(): array {
    return $this->variations;
  }

  /**
   * @param array $variations
   */
  public function setVariations(array $variations): void {
    $this->variations = $variations;
  }

  /**
   * Get the resource type.
   *
   * @return string
   *   Return the resource type.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Set the resource type.
   *
   * @param mixed $type
   *   The resource type value.
   */
  public function setType(string $type): void {
    $this->type = $type;
  }

  /**
   * Get the resource properties.
   *
   * @return array
   *   An array of properties keyed by variation name.
   */
  public function getProperties(): array {
    return $this->properties;
  }

  /**
   * Get resource property by variation.
   *
   * @param string $name
   *   The resource variation name.
   */
  public function getPropertiesByVariation(string $name): array {
    return $this->getProperties()[$name] ?? [];
  }

  /**
   * Set resource properties.
   *
   * @param array $properties
   *   An array of properties.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntityResourceDefinition
   */
  public function setProperties(array $properties): self {
    $this->properties = $properties;

    return $this;
  }

}
