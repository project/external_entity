<?php

declare(strict_types=1);

namespace Drupal\external_entity;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\external_entity\Annotation\ExternalEntityRenderType;
use Drupal\external_entity\Contracts\ExternalEntityRenderTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityRenderTypeManagerInterface;

/**
 * Define the render type manager.
 */
class RenderTypeManager extends DefaultPluginManager implements ExternalEntityRenderTypeManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/ExternalEntity/RenderType',
      $namespaces,
      $module_handler,
      ExternalEntityRenderTypeInterface::class,
      ExternalEntityRenderType::class
    );
    $this->setCacheBackend(
      $cache_backend,
      'external_entity_render_type'
    );
    $this->alterInfo('external_entity_render_type_info');
  }

  /**
   * {@inheritDoc}
   */
  public function getDefinitionOptions(): array {
    $options = [];

    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$plugin_id] = $definition['label'];
    }

    return $options;
  }

}
