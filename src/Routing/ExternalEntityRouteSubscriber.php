<?php

declare(strict_types=1);

namespace Drupal\external_entity\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\external_entity\Form\ViewUI\AddHandler;

/**
 * Declare the external entity route subscriber.
 */
class ExternalEntityRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('views_ui.form_add_handler')) {
      $class = AddHandler::class;
      $route->setDefault('_controller', "{$class}::getForm");
    }
  }

}
