<?php

declare(strict_types=1);

namespace Drupal\external_entity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define the external entity authentication type.
 *
 * @Annotation
 */
class ExternalEntityAuthenticationType extends Plugin {

  /**
   * @var string
   */
  public $id;

  /**
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;
}
