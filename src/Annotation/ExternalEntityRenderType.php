<?php

declare(strict_types=1);

namespace Drupal\external_entity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define the external entity render type.
 *
 * @Annotation
 */
class ExternalEntityRenderType extends Plugin {

  /**
   * Render type identifier.
   *
   * @var string
   */
  public $id;

  /**
   * Render type label.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
