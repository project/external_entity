<?php

declare(strict_types=1);

namespace Drupal\external_entity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define the external entity connection type.
 *
 * @Annotation
 */
class ExternalEntityConnectionType extends Plugin {

  /**
   * Connection type identifier.
   *
   * @var string
   */
  public $id;

  /**
   * Connection type label.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
