<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin;

use Drupal\Core\Form\FormStateInterface;

trait PluginFormTrait {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    // Intentionally left empty as it's not required.
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $this->setConfiguration($form_state->cleanValues()->getValues());
  }

}
