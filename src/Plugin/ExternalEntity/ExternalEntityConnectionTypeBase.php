<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\ExternalEntity;

use Drupal\Core\Plugin\PluginBase;
use Drupal\external_entity\Plugin\PluginFormTrait;
use Drupal\external_entity\Plugin\PluginConfigurableTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\external_entity\Contracts\ExternalEntityConnectionTypeInterface;

/**
 * Define the external entity connection type base class.
 */
abstract class ExternalEntityConnectionTypeBase extends PluginBase implements ExternalEntityConnectionTypeInterface {

  use PluginFormTrait;
  use PluginConfigurableTrait;

  /**
   * External entity connection type constructor.
   *
   * @param array $configuration
   *   An array of plugin configurations.
   * @param $plugin_id
   *   The plugin identifier.
   * @param $plugin_definition
   *   The plugin definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }
}
