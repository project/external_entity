<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\ExternalEntity;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\external_entity\Plugin\PluginFormTrait;
use Drupal\external_entity\Plugin\PluginConfigurableTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\external_entity\Contracts\ExternalEntityRenderTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityResourceDisplayInterface;

/**
 * Define the external entity formatter type base class.
 */
abstract class ExternalEntityRenderTypeBase extends PluginBase implements ExternalEntityRenderTypeInterface {

  use PluginFormTrait;
  use PluginConfigurableTrait;
  use CacheableDependencyTrait;

  /**
   * @var \Drupal\external_entity\Contracts\ExternalEntityResourceDisplayInterface
   */
  protected $externalEntityResourceDisplay;

  /**
   * External entity render type constructor.
   *
   * @param array $configuration
   *   An array of plugin configurations.
   * @param $plugin_id
   *   The plugin identifier.
   * @param $plugin_definition
   *   The plugin definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function setExternalEntityDisplay(
    ExternalEntityResourceDisplayInterface $display
  ): ExternalEntityRenderTypeInterface {
    $this->externalEntityResourceDisplay = $display;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getRenderModes(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTagsToInvalidate(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags(): array {
    return $this->getCacheTagsToInvalidate();
  }

}
