<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\ExternalEntity;

use Drupal\external_entity\Contracts\ExternalEntityConnectionInterface;

/**
 * Define the external entity connection aware trait.
 */
trait ExternalEntityConnectionAwareTrait {

  /**
   * @var \Drupal\external_entity\Entity\ExternalEntityConnection
   */
  protected $connection;

  /**
   * Set the external entity connection.
   *
   * @param \Drupal\external_entity\Contracts\ExternalEntityConnectionInterface $connection
   *
   * @return void
   */
  public function setConnection(
     ExternalEntityConnectionInterface $connection
  ): void {
    $this->connection = $connection;
  }

  /**
   * Check to see if the external entity connection exist.
   *
   * @return bool
   *   Return TRUE if the connection exist; otherwise FALSE.
   */
  protected function hasConnection(): bool {
    return isset($this->connection);
  }

  /**
   * Get the external entity connection.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityConnectionInterface
   *   The external entity connection.
   */
  protected function getConnection(): ExternalEntityConnectionInterface {
    return $this->connection;
  }
}
