<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\ExternalEntity\AuthenticationType;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entity\Annotation\ExternalEntityAuthenticationType;
use Drupal\external_entity\Contracts\ExternalEntityConnectionAwareInterface;
use Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeInterface;
use Drupal\external_entity\Plugin\ExternalEntity\ExternalEntityConnectionAwareTrait;
use Drupal\external_entity\Plugin\ExternalEntity\ExternalEntityAuthenticationTypeBase;

/**
 * @ExternalEntityAuthenticationType(
 *   id = "basic_authentication",
 *   label = @Translation("Basic Authentication")
 * )
 */
class BasicAuthenticationType extends ExternalEntityAuthenticationTypeBase implements ExternalEntityAuthenticationTypeInterface, ExternalEntityConnectionAwareInterface {

  use ExternalEntityConnectionAwareTrait;

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'username' => NULL,
      'password' => NULL,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#required' => TRUE,
      '#default_value' => $this->getUsername()
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Input the basic auth password. <br/>
        <strong>NOTE:</strong> If password has been saved, it will not be shown
        on the edit screen. <br/>Only input a new password when you want to
        update the existing one.'),
      '#required' => !$this->hasPassword(),
      '#default_value' => $this->getPassword()
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    if (
      empty($form_state->getValue('password'))
      && $password = $this->getPassword()
    ) {
      $form_state->setValue('password', $password);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function alterRequestOptions(
    array &$options
  ): void {
    parent::alterRequestOptions($options);
    $options['auth'] = [
      $this->getUsername(),
      $this->getPassword(),
    ];
  }

  /**
   * Get authentication username.
   *
   * @return string|null
   *   The authenticated username.
   */
  protected function getUsername(): ?string {
    return $this->getConfiguration()['username'] ?? NULL;
  }

  /**
   * Has authentication password.
   *
   * @return bool
   *   Return TRUE if password has been set; otherwise FALSE.
   */
  protected function hasPassword(): bool {
    return !empty($this->getPassword()) ;
  }

  /**
   * Get authentication password.
   *
   * @return string|null
   *   The authenticated password.
   */
  protected function getPassword(): ?string {
    $configuration = $this->getConfiguration();
    return !empty($configuration['password'])
      ? $configuration['password']
      : $this->getOriginalPassword();
  }

  /**
   * Get the original authentication password.
   *
   * @return string|null
   *   The original authentication password.
   */
  protected function getOriginalPassword(): ?string {
    if (
      ($instance = $this->connection->getOriginal())
      && $authentication = $instance->createAuthenticationTypeInstance()
    ) {
      return $authentication->getConfiguration()['password'];
    }

    return NULL;
  }

}
