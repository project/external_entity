<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\ExternalEntity;

use Drupal\Core\Plugin\PluginBase;
use Drupal\external_entity\Plugin\PluginFormTrait;
use Drupal\external_entity\Plugin\PluginConfigurableTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeInterface;

/**
 * Define the external entity authentication type base plugin class.
 */
abstract class ExternalEntityAuthenticationTypeBase extends PluginBase implements ExternalEntityAuthenticationTypeInterface {

  use PluginFormTrait;
  use PluginConfigurableTrait;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function alterRequestOptions(array &$options): void {}
}
