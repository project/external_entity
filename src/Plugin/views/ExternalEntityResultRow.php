<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\views;

use Drupal\views\ResultRow;
use Drupal\external_entity\Definition\ExternalEntityDefaultDefinition;

/**
 * Define the external entity result row.
 */
class ExternalEntityResultRow extends ResultRow {

  /**
   * @var \Drupal\external_entity\Definition\ExternalEntityDefaultDefinition
   */
  public $definition;

  /**
   * Has external entity definition.
   *
   * @return bool
   *   Return TRUE if the definition has been defined; otherwise FALSE.
   */
  public function hasDefinition(): bool {
    return isset($this->definition)
      && $this->definition instanceof ExternalEntityDefaultDefinition;
  }
}
