<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\views\row;

use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\row\RowPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\external_entity\Contracts\ExternalEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\external_entity\Plugin\views\query\ExternalEntityQuery;

/**
 * @ViewsRow(
 *   id = "external_entity",
 *   title = @Translation("External Entity"),
 *   base = {"external_entity"}
 * )
 */
class ExternalEntityRow extends RowPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * External entity row constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;

  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defineOptions(): array {
    return [
      'view_mode' => ['default' => 'default']
    ] + parent::defineOptions();
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View Mode'),
      '#options' => $this->getExternalEntityViewOptions(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $options = $this->getExternalEntityViewOptions();
    return $options[$this->options['view_mode']] ?? $this->t('No view mode selected');
  }

  /**
   * {@inheritDoc}
   */
  public function render($row): array {
    $output = [];

    if ($entity = $this->getEntity($row)) {
      /** @var \Drupal\external_entity\Entity\ExternalEntityViewBuilder $builder */
      $builder = $this->entityTypeManager->getViewBuilder(
        $entity->getEntityTypeId()
      );

      $output = $builder->view($entity, $this->options['view_mode']);
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();

    $view_mode = $this->entityTypeManager->getStorage('entity_view_mode')
      ->load("{$this->getExternalEntityResource()}.{$this->options['view_mode']}");

    if ($view_mode) {
      $dependencies[$view_mode->getConfigDependencyKey()][] = $view_mode->getConfigDependencyName();
    }

    return $dependencies;
  }

  /**
   * Get the display handler query plugin.
   *
   * @return \Drupal\external_entity\Plugin\views\query\ExternalEntityQuery
   *   The external entity query instance.
   */
  protected function getQueryPlugin(): ExternalEntityQuery {
    return $this->displayHandler->getPlugin('query');
  }

  /**
   * Get the external entity resource.
   *
   * @return string|null
   *   The external entity resource.
   */
  protected function getExternalEntityResource(): ?string {
    return $this->getQueryPlugin()->options['resource'] ?? NULL;
  }

  /**
   * Get external entity view mode options.
   *
   * @return array
   *   An array of external entity view mode options.
   */
  protected function getExternalEntityViewOptions(): array {
    return $this->entityDisplayRepository->getViewModeOptions(
      $this->getExternalEntityResource()
    );
  }

  /**
   * Get external entity instance.
   *
   * @param \Drupal\views\ResultRow $row
   *   The views result row instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityInterface|null
   */
  protected function getEntity(ResultRow $row): ?ExternalEntityInterface {
    if (isset($row->_entity) && $row->_entity instanceof ExternalEntityInterface) {
      return $row->_entity;
    }

    return NULL;
  }

}
