<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\views\field;

use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterPluginManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the external entity field plugin.
 *
 * @ViewsField("external_entity_field")
 */
class ExternalEntityField extends FieldPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;
  /**
   * @var \Drupal\Core\Field\FormatterPluginManager
   */
  protected $fieldFormatterManager;

  /**
   * Constructor for the external entity field views handler.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    FormatterPluginManager $field_formatter_manager,
    FieldTypePluginManagerInterface $field_type_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->fieldTypeManager = $field_type_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldFormatterManager = $field_formatter_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.formatter'),
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defineOptions(): array {
    $options = parent::defineOptions();
    $options['field_formatter']['default'] = $this->getFieldTypeDefaultFormatter();
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(
    &$form,
    FormStateInterface $form_state
  ): void {
    parent::buildOptionsForm($form, $form_state);

    $form['field_formatter'] = [
      '#type' => 'select',
      '#title' => $this->t('Field Formatter'),
      '#required' => TRUE,
      '#options' => $this->getFieldTypeFormatterOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $this->options['field_formatter']
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function render(ResultRow $values) {
    return $this->getValue($values);
  }

  /**
   * {@inheritDoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
    /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
    $entity = $this->getEntity($values);
    $field_name = isset($field) ? $this->aliases[$field] : $this->field_alias;

    if (!$entity->hasField($field_name)) {
      return [];
    }
    $field_item = $entity->get($field_name);

    if ($field_item->isEmpty()) {
      return [];
    }
    $display_options = [];

    if ($field_formatter = $this->getFieldFormatter()) {
      $display_options['type'] = $field_formatter;
    }

    return $this->externalEntityViewBuilder()->viewField(
      $field_item,
      $display_options
    );
  }

  /**
   * Get the external entity field type.
   *
   * @return string|null
   *   The external entity field type.
   */
  protected function getFieldType(): ?string {
    return $this->definition['field_type'] ?? NULL;
  }

  /**
   * Get the field formatter.
   *
   * @return string|null
   *   The field formatter type.
   */
  protected function getFieldFormatter(): ?string {
    return $this->options['field_formatter'];
  }

  /**
   * Get type formatter options.
   *
   * @return array
   *   An array of the formatter options.
   */
  protected function getFieldTypeFormatterOptions(): array {
    return $this->fieldFormatterManager->getOptions(
      $this->getFieldType()
    );
  }

  /**
   * Get field type definition.
   *
   * @return array
   *   An array of the field definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFieldTypeDefinition(): array {
    return $this->fieldTypeManager->getDefinition($this->getFieldType());
  }

  /**
   * Get the external entity view builder.
   *
   * @return \Drupal\Core\Entity\EntityViewBuilderInterface
   *   The external entity view builder.
   */
  protected function externalEntityViewBuilder(): EntityViewBuilderInterface {
    return $this->entityTypeManager->getViewBuilder('external_entity');
  }

  /**
   * Get the field type default formatter type.
   *
   * @return string|null
   *   The field default formatter type.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFieldTypeDefaultFormatter(): ?string {
    return $this->getFieldTypeDefinition()['default_formatter'] ?? NULL;
  }

}
