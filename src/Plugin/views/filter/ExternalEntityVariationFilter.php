<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\views\filter;

use Drupal\views\ViewExecutable;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\external_entity\Contracts\ExternalEntityTypeInterface;
use Drupal\external_entity\Plugin\views\query\ExternalEntityQuery;

/**
 * Define an external entity variation filter.
 *
 * @ViewsFilter("external_entity_variation_filter")
 */
class ExternalEntityVariationFilter extends FilterPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct for the view filter plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function init(
    ViewExecutable $view,
    DisplayPluginBase $display,
    array &$options = NULL
  ): void {
    parent::init($view, $display, $options);
    $variation = $this->options['variation'] ?? [];
    $definition = $this->getExternalEntityResourceDefinition();

    $this->operator = 'IN';
    $this->value = array_values($variation);
    $this->field = $definition->getKey('bundle');
  }

  /**
   * {@inheritDoc}
   */
  public function adminSummary(): ?string {
    $value = $this->value;

    if (!is_array($value)) {
      return NULL;
    }

    return implode(', ', $value);
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(
    &$form,
    FormStateInterface $form_state
  ): void {
    parent::buildOptionsForm($form, $form_state);

    $form['variation'] = [
      '#type' => 'select',
      '#title' => $this->t('Variation'),
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#description' => $this->t(
        'Select the external entity resource variations to show.'
      ),
      '#options' => $this->getExternalEntityVariationOptions(),
      '#default_value' => $this->options['variation'],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function query(): void {
    $this->query->addWhere($this->options['group'], $this->field, $this->value, $this->operator);
  }

  /**
   * {@inheritDoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['variation'] = ['default' => []];

    return $options;
  }

  /**
   * Get the view query instance.
   *
   * @return \Drupal\external_entity\Plugin\views\query\ExternalEntityQuery|null
   */
  protected function viewsQuery(): ExternalEntityQuery {
    $query = $this->view->getQuery();

    if (!$query instanceof ExternalEntityQuery) {
      throw new \RuntimeException(
        'The views query is invalid.'
      );
    }

    return $query;
  }

  /**
   * Get external entity type.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityTypeInterface
   *   The external entity type instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getExternalEntityType(): ExternalEntityTypeInterface {
    return $this->externalEntityTypeStorage()->load(
      $this->viewsQuery()->externalEntityTypeId()
    );
  }

  /**
   * Get external entity resource definition.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getExternalEntityResourceDefinition(): EntityTypeInterface {
    return $this->entityTypeManager->getDefinition(
      $this->viewsQuery()->externalEntityResource()
    );
  }

  /**
   * Get external entity variations.
   *
   * @return array
   *   An array of external entity variations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getExternalEntityVariations(): array {
    return $this->getExternalEntityType()->getResourceVariations(
      $this->viewsQuery()->externalEntityResource()
    );
  }

  /**
   * Get external entity variation options.
   *
   * @return array
   *   An array of external entity variation options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getExternalEntityVariationOptions(): array {
    $options = [];

    foreach ($this->getExternalEntityVariations() as $variation => $info) {
      if (!isset($info['label'])) {
        continue;
      }
      $options[$variation] = $info['label'];
    }

    return $options;
  }

  /**
   * Get external entity type storage instance.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   *   The external entity type storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function externalEntityTypeStorage(): ConfigEntityStorageInterface {
    return $this->entityTypeManager->getStorage(
      'external_entity_type'
    );
  }

}
