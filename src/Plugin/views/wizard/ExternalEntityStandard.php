<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin\views\wizard;

use Drupal\views\Views;
use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entity\ExternalEntityOptions;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\views\Plugin\views\wizard\WizardPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the external entity standard.
 *
 * @ViewsWizard(
 *   id = "external_entity_standard",
 *   title = @Translation("External Entity"),
 *   base_table = "external_entity"
 * )
 */
class ExternalEntityStandard extends WizardPluginBase {

  /**
   * @var \Drupal\external_entity\ExternalEntityOptions
   */
  protected $externalEntityOptions;

  /**
   * External entity standard constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\external_entity\ExternalEntityOptions $external_entity_options
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The entity bundle info service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ExternalEntityOptions $external_entity_options,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $bundle_info_service
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $bundle_info_service
    );
    $this->entityTypeId = 'external_entity_type';
    $this->entityType = $entity_type_manager->getDefinition(
      $this->entityTypeId
    );
    $this->externalEntityOptions = $external_entity_options;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('external_entity.options'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildDisplayOptions(
    $form,
    FormStateInterface $form_state
  ): array {
    $display_options = parent::buildDisplayOptions($form, $form_state);

    $display_options['default']['query']['options'] = [
      'type_of' => $form_state->getValue(['show', 'type_of']),
      'resource' => $form_state->getValue(['show', 'resource'])
    ];

    return $display_options;
  }

  /**
   * {@inheritDoc}
   */
  protected function defaultDisplayOptions(): array {
    $display_options = [];
    $display_options['access']['type'] = 'none';
    $display_options['cache']['type'] = 'tag';
    $display_options['query']['type'] = 'views_query';
    $display_options['exposed_form']['type'] = 'basic';
    $display_options['pager']['type'] = 'mini';
    $display_options['style']['type'] = 'default';
    $display_options['row']['type'] = 'fields';

    foreach ($display_options as &$options) {
      $options['options'] = [];
    }

    return $display_options;
  }

  /**
   * {@inheritDoc}
   */
  protected function buildFilters(&$form, FormStateInterface $form_state): void {
    $options = $this->externalEntityOptions;
    $option_types = $options->types();

    $form['displays']['show']['type_of'] = [
      '#type' => 'select',
      '#title' => $this->t('type of'),
      '#options' => $option_types,
      '#empty_option' => $this->t('- Select -')
    ];
    $show_form = &$form['displays']['show'];
    $external_type_id = static::getSelected(
      $form_state, ['show', 'type_of'], NULL, $show_form['type_of']
    );

    if (isset($option_types[$external_type_id])) {
      $form['displays']['show']['type_of']['#default_value'] = $external_type_id;
    }
    views_ui_add_ajax_trigger($show_form, 'type_of', ['displays']);

    if (isset($external_type_id) && !empty($external_type_id)) {
      $form['displays']['show']['resource'] = [
        '#type' => 'select',
        '#title' => $this->t('with resource'),
        '#options' =>$options->resources($external_type_id),
        '#empty_option' => $this->t('- Select -')
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function alterDisplayOptions(
    &$display_options,
    $form,
    FormStateInterface $form_state
  ): void {
    parent::alterDisplayOptions($display_options, $form, $form_state);

    if ($default_field = $this->buildDefaultField($form_state)) {
      $table = $this->base_table;
      $data = Views::viewsData()->get($table);
      if (isset($data[$default_field])) {
        $display_options['default']['fields'][$default_field] = [
          'table' => $table,
          'field' => $default_field,
          'id' => $default_field,
          'plugin_id' => $data[$default_field]['field']['id']
        ];
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function buildSorts(&$form, FormStateInterface $form_state): void {
    // Intentionally left empty to remove sorting options.
  }

  /**
   * Build the default field based on external entity type and resource.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return string|null
   *   The default field.
   */
  protected function buildDefaultField(
    FormStateInterface $form_state
  ): ?string {
    $field = NULL;

    if (
      ($type = $form_state->getValue(['show', 'type_of']))
      && $resource = $form_state->getValue(['show', 'resource'])
    ) {
      $field = "{$type}__{$resource}__id";
    }

    return $field;
  }

}
