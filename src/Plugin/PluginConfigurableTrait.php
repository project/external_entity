<?php

declare(strict_types=1);

namespace Drupal\external_entity\Plugin;

use Drupal\Component\Utility\NestedArray;

/**
 * Define the plugin configurable trait.
 */
trait PluginConfigurableTrait {

  /**
   * Gets default configuration for this plugin.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * Gets this plugin's configuration.
   *
   * @return array
   *   An array of this plugin's configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * Sets the configuration for this plugin instance.
   *
   * @param array $configuration
   *   An associative array containing the plugin's configuration.
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $this->configuration,
      $configuration
    );
  }

}
