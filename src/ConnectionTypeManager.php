<?php

declare(strict_types=1);

namespace Drupal\external_entity;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\external_entity\Annotation\ExternalEntityConnectionType;
use Drupal\external_entity\Contracts\ExternalEntityConnectionTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityConnectionTypeManagerInterface;

/**
 * Define the connection type manager.
 */
class ConnectionTypeManager extends DefaultPluginManager implements ExternalEntityConnectionTypeManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/ExternalEntity/ConnectionType',
      $namespaces,
      $module_handler,
      ExternalEntityConnectionTypeInterface::class,
      ExternalEntityConnectionType::class
    );
    $this->setCacheBackend(
      $cache_backend,
      'external_entity_connection_type'
    );
    $this->alterInfo('external_entity_connection_type_info');
  }

  /**
   * {@inheritDoc}
   */
  public function getDefinitionOptions(): array {
    $options = [];

    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$plugin_id] = $definition['label'];
    }

    return $options;
  }

}
