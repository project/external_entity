<?php

declare(strict_types=1);

namespace Drupal\external_entity\Views;

use Drupal\views\ViewEntityInterface;
use Drupal\views\ViewsDataHelper as ViewsDataHelperBase;
use Drupal\external_entity\Plugin\views\query\ExternalEntityQuery;

/**
 * Define the external entity views data helper.
 */
class ViewsDataHelper extends ViewsDataHelperBase {

  /**
   * Fetches a list of filtered fields available.
   *
   * @param array|string $base
   *   A list or a single base_table, for example node.
   * @param string $type
   *   The handler type, for example field or filter.
   * @param \Drupal\views\ViewEntityInterface $view
   *   The view entity instance.
   * @param bool $grouping
   *   Should the result grouping by its 'group' label.
   * @param string|null $sub_type
   *   An optional sub type. E.g. Allows making an area plugin available for
   *   header only, instead of header, footer, and empty regions.
   *
   * @return array
   *   A keyed array of in the form.
   */
  public function fetchFilteredFields(
    array $base,
    string $type,
    ViewEntityInterface $view,
    bool $grouping = FALSE,
    ?string $sub_type = NULL
  ): array {
    $fields = $this->fetchFields($base, $type, $grouping, $sub_type);
    $views_query = $view->getExecutable()->getQuery();

    if (!$views_query instanceof ExternalEntityQuery) {
      return $fields;
    }

    return array_filter($fields, static function ($key) use ($views_query) {
      $type = $views_query->externalEntityTypeId();
      $resource = $views_query->externalEntityResource();
      return
        strpos($key, 'views.') === 0
        || $key === 'external_entity.entity_browser_select'
        || $key === 'external_entity.external_entity_variation'
        || strpos($key, "external_entity.{$type}__{$resource}__") !== FALSE;
    }, ARRAY_FILTER_USE_KEY);
  }
}
