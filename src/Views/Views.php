<?php

declare(strict_types=1);

namespace Drupal\external_entity\Views;

/**
 * Define the views static services.
 */
class Views {

  /**
   * Views data helper service.
   *
   * @return \Drupal\external_entity\Views\ViewsDataHelper
   */
  public static function viewsDataHelper(): ViewsDataHelper {
    return \Drupal::service('external_entity.views_data_helper');
  }

}
