<?php

declare(strict_types=1);

namespace Drupal\external_entity\Cache;

use Drupal\Core\Cache\DatabaseBackend;
use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;

/**
 * Define the external entity database backend.
 */
class ExternalEntityDatabaseBackend extends DatabaseBackend implements CacheTagsInvalidatorInterface {

  /**
   * {@inheritDoc}
   */
  public function invalidateTags(array $tags): void {
    assert(Inspector::assertAllStrings($tags));

    if ($cache_ids = $this->getCacheIdsByTags($tags)) {
      $this->invalidateMultiple($cache_ids);
    }
  }

  /**
   * Get all cache IDs by cache tags.
   *
   * @param array $tags
   *   An array of cache tags.
   *
   * @return array
   *   An array of cache IDs.
   */
  protected function getCacheIdsByTags(array $tags): array {
    $cache_ids = [];

    if ($this->ensureBinExists()) {
      $connection = $this->connection;
      $query = $connection->select($connection->escapeDatabase($this->bin), 'c')
        ->fields('c', ['cid']);

      foreach ($tags as $tag) {
        if (!is_string($tag)) {
          continue;
        }
        $query->condition('tags', "%{$connection->escapeLike($tag)}%", 'LIKE');
      }

      $cache_ids = $query->execute()->fetchCol();
    }

    return $cache_ids;
  }

}
