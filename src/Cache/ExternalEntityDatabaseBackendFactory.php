<?php

declare(strict_types=1);

namespace Drupal\external_entity\Cache;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\DatabaseBackendFactory;

/**
 * Define the external entity database factory.
 */
class ExternalEntityDatabaseBackendFactory extends DatabaseBackendFactory {

  /**
   * {@inheritDoc}
   */
  public function get($bin): CacheBackendInterface {
    $max_rows = $this->getMaxRowsForBin($bin);

    return new ExternalEntityDatabaseBackend(
      $this->connection,
      $this->checksumProvider,
      $bin,
      $max_rows
    );
  }
}
