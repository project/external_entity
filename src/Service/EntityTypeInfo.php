<?php

declare(strict_types=1);

namespace Drupal\external_entity\Service;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;

/**
 * Define the entity type information service.
 */
class EntityTypeInfo {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type information constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * @return array
   */
  public function getEntityTypeDefinitions(): array {
    return $this->entityTypeManager->getDefinitions();
  }

  /**
   * @return array
   */
  public function getEntityTypeOptions(): array {
    $options = [];

    foreach ($this->getEntityTypeDefinitions() as $name => $definition) {
      if (!$definition instanceof ContentEntityType) {
        continue;
      }
      $options[$name] = $definition->getLabel();
    }

    return $options;
  }

  public function getEntityBundleOptions(string $entity_type_id): array {
    $options = [];

    foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $name => $bundle) {
      if (!isset($bundle['label'])) {
        continue;
      }
      $options[$name] = $bundle['label'];
    }

    return $options;
  }

  /**
   * @param string $entity_type_id
   * @param string $bundle
   *
   * @return array
   */
  public function getEntityFieldDefinitions(
    string $entity_type_id,
    string $bundle = 'default'
  ): array {
    return $this->entityFieldManager->getFieldDefinitions(
      $entity_type_id,
      $bundle
    );
  }

  /**
   * @param string $entity_type_id
   * @param string $bundle
   *
   * @return array
   */
  public function getEntityFieldOptions(
    string $entity_type_id,
    string $bundle
  ): array {
    $options = [];

    foreach ($this->getEntityFieldDefinitions($entity_type_id, $bundle) as $name => $definition) {
      if ($definition->isComputed()) {
        continue;
      }
      $options[$name] = $definition->getLabel();
    }

    ksort($options);

    return $options;
  }

  /**
   * @param string $entity_type_id
   *
   * @return array
   */
  public function getEntityViewDisplayOptions(
    string $entity_type_id
  ): array {
    return $this->entityDisplayRepository->getViewModeOptions($entity_type_id);
  }

}
