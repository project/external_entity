<?php

declare(strict_types=1);

namespace Drupal\external_entity;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\external_entity\Annotation\ExternalEntityAuthenticationType;
use Drupal\external_entity\Contracts\ExternalEntityAuthenticationInterface;
use Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeInterface;
use Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeManagerInterface;

/**
 * Define the authentication type plugin manager.
 */
class AuthenticationTypeManager extends DefaultPluginManager implements ExternalEntityAuthenticationTypeManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/ExternalEntity/AuthenticationType',
      $namespaces,
      $module_handler,
      ExternalEntityAuthenticationTypeInterface::class,
      ExternalEntityAuthenticationType::class
    );
    $this->setCacheBackend(
      $cache_backend,
      'external_entity_authentication_type'
    );
    $this->alterInfo('external_entity_authentication_type_info');
  }

  /**
   * {@inheritDoc}
   */
  public function getDefinitionOptions(): array {
    $options = [];

    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$plugin_id] = $definition['label'];
    }

    return $options;
  }

}
