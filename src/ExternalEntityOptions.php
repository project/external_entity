<?php

declare(strict_types=1);

namespace Drupal\external_entity;

use Drupal\Core\Entity\EntityTypeManagerInterface;

class ExternalEntityOptions {

  protected $externalEntityTypeStorage;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->externalEntityTypeStorage = $entity_type_manager->getStorage(
      'external_entity_type'
    );
  }

  /**
   * Get external entity type options.
   *
   * @return array
   *   An array of external entity types.
   */
  public function types(): array {
    $options = [];

    foreach ($this->externalEntityTypeStorage->loadMultiple() as $id => $type) {
      $options[$id] = $type->label();
    }

    return $options;

  }

  /**
   * Get external entity resource options.
   *
   * @param string $entity_type_id
   *
   * @return array
   *   An array of external entity resources.
   */
  public function resources(string $entity_type_id): array {
    $options = [];
    $entity = $this->externalEntityTypeStorage->load($entity_type_id);

    /** @var \Drupal\external_entity\Definition\ExternalEntityResourceDefinition $resource */
    foreach ($entity->getResourceDefinitions() as $name => $resource) {
      $options[$name] = $resource->getLabel();
    }

    return $options;
  }

}
