<?php

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Entity\EntityInterface;

/**
 * Define external entity interface.
 */
interface ExternalEntityInterface extends EntityInterface {

  /**
   * Get the external entity resource path.
   *
   * @return string|null
   *   The external entity resource path.
   */
  public function getPath(): ?string;

  /**
   * Get the external entity resource.
   *
   * @return string
   *   The external entity resource.
   */
  public function getResource(): string;

  /**
   * Get the external entity resource variation.
   *
   * @return string
   *   The external entity resource variation.
   */
  public function getVariation(): string;

  /**
   * Get the bundle entity type identifier.
   *
   * @return string|null
   *   The external entity type identifier.
   */
  public function getBundleEntityTypeId(): ?string;

  /**
   * Get the bundle type instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityTypeInterface
   *   The external entity type instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBundleEntityType(): ?ExternalEntityTypeInterface;

  /**
   * Get the external entity resource properties.
   *
   * @param bool $include_alteration
   *   Include the external entity property alterations.
   *
   * @return array
   *   The external entity resource properties.
   *
   * @throws \Drupal\external_entity\Exception\NotFoundResourceDisplayException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getProperties(bool $include_alteration): array;

  /**
   * Get external entity definitions.
   *
   * @param bool $include_alteration
   *
   * @return array
   *   An array of external entity property definitions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPropertyDefinitions(bool $include_alteration = TRUE): array;

  /**
   * Get external entity type resource properties.
   *
   * @return array
   *   An array of the external entity type properties.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\external_entity\Exception\InvalidResourceException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResourceProperties(): array;

}
