<?php

namespace Drupal\external_entity\Contracts;

/**
 * Define the external entity type config entity interface.
 */
interface ExternalEntityTypeConfigEntityInterface {

  /**
   * Get the external entity type ID.
   *
   * @return string|NULL
   */
  public function getExternalEntityTypeId(): ?string;

  /**
   * Set external entity type instance.
   *
   * @param string $external_entity_type_id
   *   The external entity type ID.
   *
   * @return $this
   *   The current object.
   */
  public function setExternalEntityTypeId(
    string $external_entity_type_id
  ): ExternalEntityTypeConfigEntityInterface;

  /**
   * Get external entity type instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityTypeInterface
   *   The external entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function externalEntityType(): ?ExternalEntityTypeInterface;

}
