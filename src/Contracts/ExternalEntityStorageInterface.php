<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\external_entity\Definition\ExternalEntityDefaultDefinition;

/**
 * Define the external entity storage interface.
 */
interface ExternalEntityStorageInterface {

  /**
   * Get the resource query service.
   *
   * @param string $resource
   *   The external entity resource.
   * @param string $entity_type_id
   *   The external entity type identifier.
   * @param string $conjunction
   *   The query conjunction.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The external resource query instance.
   */
  public function getResourceQuery(
    string $resource,
    string $entity_type_id,
    string $conjunction
  ): QueryInterface;

  /**
   * Create external entity from definition.
   *
   * @param string $uuid
   *   The external entity UUID.
   * @param string $external_entity_type
   *   The external entity type ID.
   * @param \Drupal\external_entity\Definition\ExternalEntityDefaultDefinition $definition
   *   The eternal entity definition.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityInterface
   */
  public function createEntityFromDefinition(
    string $external_entity_type,
    ExternalEntityDefaultDefinition $definition
  ): ExternalEntityInterface;

}
