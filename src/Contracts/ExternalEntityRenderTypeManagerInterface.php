<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Define the external entity render type manager interface.
 */
interface ExternalEntityRenderTypeManagerInterface extends PluginManagerInterface {

  /**
   * Get plugin definition options.
   *
   * @return array
   *   An array or definition options.
   */
  public function getDefinitionOptions(): array;
}
