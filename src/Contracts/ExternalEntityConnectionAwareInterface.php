<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

/**
 * Define the external entity authentication type aware interface.
 */
interface ExternalEntityConnectionAwareInterface {

  /**
   * Set the external entity authentication type.
   *
   * @param \Drupal\external_entity\Contracts\ExternalEntityConnectionInterface $connection
   *   The external entity connection.
   *
   * @return void
   */
  public function setConnection(
    ExternalEntityConnectionInterface $connection
  ): void;
}
