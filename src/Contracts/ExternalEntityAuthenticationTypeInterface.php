<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Define the external entity authentication type interface.
 */
interface ExternalEntityAuthenticationTypeInterface extends ConfigurableInterface, PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * Alter the HTTP client request options.
   *
   * @param array $options
   *   An array of HTTP client request options.
   *
   * @return void
   */
  public function alterRequestOptions(array &$options): void;

}
