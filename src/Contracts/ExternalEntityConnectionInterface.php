<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Define the external entity connection interface.
 */
interface ExternalEntityConnectionInterface extends ConfigEntityInterface {

  /**
   * Determine if the connection exist.
   *
   * @param string $id
   *   The connection ID to verify.
   *
   * @return bool
   */
  public function exists(string $id): bool;

  /**
   * Get the connection type id.
   *
   * @return string
   *   Return the connection type id.
   */
  public function connectionTypeId(): ?string;

  /**
   * Get the connection type setting.
   *
   * @return array
   *   An array for the connection type setting.
   */
  public function connectionSettings(): array;

  /**
   * Get connection authentication type id.
   *
   * @return string
   *   The connection authentication type id.
   */
  public function authenticationTypeId(): ?string;

  /**
   * Get connection authentication type setting.
   *
   * @return array
   *   An array of the authentication type setting.
   */
  public function authenticationSettings(): array;

  /**
   * Get the original connection entity.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityConnectionInterface|null
   *   The original connection entity.
   */
  public function getOriginal(): ?ExternalEntityConnectionInterface;

  /**
   * Verify external entity connection has connection type.
   *
   * @return bool
   *   Return TRUE if connection type is valid; otherwise FALSE.
   */
  public function hasConnectionType(): bool;

  /**
   * Get the external connection type instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityConnectionTypeInterface|null
   *   The external entity connection instance.
   */
  public function connectionTypeInstance(): ?ExternalEntityConnectionTypeInterface;

  /**
   * Verify external entity connection has authentication type.
   *
   * @return bool
   *   Return TRUE if authentication type is valid; otherwise FALSE.
   */
  public function hasAuthenticationType(): bool;

  /**
   * Create authenticate type instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityAuthenticationTypeInterface|null
   *   The external entity authentication type instance.
   */
  public function createAuthenticationTypeInstance(): ?ExternalEntityAuthenticationTypeInterface;
}
