<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Define the external render type interface.
 */
interface ExternalEntityRenderTypeInterface extends ConfigurableInterface, PluginFormInterface, ContainerFactoryPluginInterface, CacheableDependencyInterface {

  /**
   * Render the external entity.
   *
   * @return array
   *   Return a renderable array.
   */
  public function render(ExternalEntityInterface $external_entity, string $mode = 'default'): array;

  /**
   * Create the rendered entity.
   *
   * @param \Drupal\external_entity\Contracts\ExternalEntityInterface $external_entity
   *   The external entity instance.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity instance object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createEntity(ExternalEntityInterface $external_entity): ?EntityInterface;

  /**
   * Set external entity resource display.
   *
   * @param \Drupal\external_entity\Contracts\ExternalEntityResourceDisplayInterface $display
   *   The external entity resource display.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityRenderTypeInterface
   */
  public function setExternalEntityDisplay(ExternalEntityResourceDisplayInterface $display): ExternalEntityRenderTypeInterface;

  /**
   * Define the render type modes.
   *
   * @return array
   *   An array of render type modes.
   */
  public function getRenderModes(): array;

  /**
   * Returns the cache tags that should be used to invalidate caches.
   *
   * This will not return additional cache tags added through addCacheTags().
   *
   * @return string[]
   *   Set of cache tags.
   *
   * @see \Drupal\Core\Cache\RefinableCacheableDependencyInterface::addCacheTags()
   * @see \Drupal\Core\Cache\CacheableDependencyInterface::getCacheTags()
   */
  public function getCacheTagsToInvalidate(): array;
}
