<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Entity\EntityInterface;

/**
 * Define the external entity type interface.
 */
interface ExternalEntityTypeInterface extends EntityInterface {

  /**
   * Get external entity storage information.
   */
  public function getStorage(): array;

  /**
   * Get storage connection identifier.
   *
   * @return string|null
   *   The storage connection ID.
   */
  public function getStorageConnectionId(): ?string;

  /**
   * Get external entity resource options.
   *
   * @return array
   *   An array of resource options keyed by various sections (e.g. resources,
   *   variations, and properties).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResourceOptions(): array;

  /**
   * Get the external entity resource definitions.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntityResourceDefinition[]
   *   An array external resource definitions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResourceDefinitions(): array;

  /**
   * Get all the resource properties.
   *
   * @return array
   *   An array of all the resource properties.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAllResourceProperties(): array;

  /**
   * Get all external entity resource alterations.
   *
   * @return \Drupal\external_entity\Entity\ExternalEntityResourceAlter[]
   *   An array of resource alteration objects.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAllResourceAlterations(): array;

  /**
   * Get external entity resource alterations by resource and variation.
   *
   * @return \Drupal\external_entity\Entity\ExternalEntityResourceAlter[]
   *   An array of resource alteration objects.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getResourceAlterations(string $resource, string $variation): array;

  /**
   * Get the external entity type properties.
   *
   * @param string $name
   *   The external resource name.
   * @param string $variation
   *   The external variation name.
   *
   * @return array
   *   An array of external entity properties.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\external_entity\Exception\InvalidResourceException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResourceProperties(string $name, string $variation): array;

  /**
   * Get the external entity connection instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityConnectionInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStorageConnection(): ExternalEntityConnectionInterface;

  /**
   * Get the external entity resource variations.
   *
   * @param string $resource
   *   The external resource name.
   *
   * @return array
   *   An array of external entity resource variations.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResourceVariations(string $resource): array;

  /**
   * Get the external entity resource display.
   *
   * @param string $resource
   *   The external resource name.
   * @param string $variation
   *   The external resource variation.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityResourceDisplayInterface|null
   *   The related resource display instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\external_entity\Exception\NotFoundResourceDisplayException
   */
  public function getResourceDisplay(string $resource, string $variation): ?ExternalEntityResourceDisplayInterface;

}
