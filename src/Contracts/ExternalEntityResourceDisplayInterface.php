<?php

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Define the external entity resource display interface.
 */
interface ExternalEntityResourceDisplayInterface extends ConfigEntityInterface {

  /**
   * Get external entity display render configurations.
   *
   * @return array
   *   An array of render configurations.
   */
  public function getRender(): array;

  /**
   * Get the external entity display resource name.
   *
   * @return string|null
   *   The display resource name.
   */
  public function getResource(): ?string;

  /**
   * Get the external entity display variation.
   *
   * @return string|null
   *   The display resource variation.
   */
  public function getVariation(): ?string;

  /**
   * Get external entity display render type.
   *
   * @return string|null
   *   The external entity render type.
   */
  public function getRenderType(): ?string;

  /**
   * Get external entity display render settings.
   *
   * @return array
   *   An array of the render settings.
   */
  public function getRenderSettings(): ?array;

  /**
   * Create the external entity render type instance.
   *
   * @return \Drupal\external_entity\Contracts\ExternalEntityRenderTypeInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createRenderInstance(): ?ExternalEntityRenderTypeInterface;

}
