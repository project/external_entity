<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\external_entity\Entity\Query\SearchQuery;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\external_entity\Definition\ExternalEntitySearchDefinition;

/**
 * Define the external connection type interface.
 */
interface ExternalEntityConnectionTypeInterface extends ConfigurableInterface, PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * Fetch resource definitions from a third-party system.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntityResourceDefinition[]
   *   An array of external entity resource definitions.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function fetchResourceDefinitions(): array;

  /**
   * Lookup definitions from a third-party system.
   *
   * @param array $ids
   *   An array of IDs to lookup.
   * @param array $options
   *   An array of options.
   *
   * @return \Drupal\external_entity\Definition\ExternalEntityDefaultDefinition[]
   *   An array of external entity definitions.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function lookupDefinitions(array $ids = [], array $options = []): array;

  /**
   * Search definitions on a third-party system.
   *
   * @param string $resource
   *   The external entity resource.
   * @param SearchQuery $query
   *   The search query object.
   */
  public function searchDefinitions(string $resource, SearchQuery $query): ?ExternalEntitySearchDefinition;

}
