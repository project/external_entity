<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;

/**
 * Define the external entity view builder interface.
 */
interface ExternalEntityViewBuilderInterface extends EntityHandlerInterface, EntityViewBuilderInterface {

  /**
   * Create the external entity proxy entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The external entity proxy entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\external_entity\Exception\NotFoundResourceDisplayException
   */
  public function createProxyEntity(ExternalEntityInterface $entity): ?EntityInterface;

}
