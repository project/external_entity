<?php

declare(strict_types=1);

namespace Drupal\external_entity\Contracts;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Define the external entity connection type manager interface.
 */
interface ExternalEntityConnectionTypeManagerInterface extends PluginManagerInterface {

  /**
   * Get plugin definition options.
   *
   * @return array
   *   An array or definition options.
   */
  public function getDefinitionOptions(): array;
}
