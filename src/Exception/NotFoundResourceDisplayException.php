<?php

declare(strict_types=1);

namespace Drupal\external_entity\Exception;

/**
 * Define the not found resource display exception.
 */
class NotFoundResourceDisplayException extends \Exception {

  /**
   * Exception constructor.

   * @param string $resource
   *   The external entity display resource.
   * @param string $variation
   *   The external entity display variation.
   */
  public function __construct(string $resource, string $variation) {
    parent::__construct(sprintf(
      'The display was not found for the %s resource with a variation of %s.',
      $resource,
      $variation
    ));
  }

}
