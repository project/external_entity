<?php

declare(strict_types=1);

namespace Drupal\external_entity\Exception;

/**
 * Define the invalid resource exception.
 */
class InvalidResourceException extends \Exception {

  /**
   * @param string $resource
   */
  public function __construct(string $resource) {
    parent::__construct(sprintf(
      'Invalid resource name %s was given.',
      $resource
    ));
  }

}
