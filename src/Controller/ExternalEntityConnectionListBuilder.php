<?php

declare(strict_types=1);

namespace Drupal\external_entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Define the external entity connection list builder.
 */
class ExternalEntityConnectionListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader(): array {
    return [
      $this->t('Name'),
      $this->t('Connection ID'),
      $this->t('Connection Type')
    ] + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\external_entity\Entity\ExternalEntityConnection $entity */
    return [
      'name' => $entity->label(),
      'label' => $entity->id(),
      'type' => $entity->connectionTypeId()
    ] + parent::buildRow($entity);
  }

}
