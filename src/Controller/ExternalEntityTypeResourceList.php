<?php

declare(strict_types=1);

namespace Drupal\external_entity\Controller;

use Drupal\Core\Url;
use Drupal\Core\Render\Element;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\external_entity\Contracts\ExternalEntityTypeInterface;

/**
 * Define the external entity manager resource list.
 */
class ExternalEntityTypeResourceList extends ControllerBase {

  /**
   * @var array
   */
  protected $alterations = [];

  /**
   * Build the external entity type resource list.
   *
   * @param \Drupal\external_entity\Contracts\ExternalEntityTypeInterface $external_entity_type
   *   The external entity type.
   *
   * @return array
   *   An array of the elements.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function build(
    ExternalEntityTypeInterface $external_entity_type
  ): array {
    $build = [];
    $external_entity_type_id = $external_entity_type->id();

    foreach ($external_entity_type->getResourceDefinitions() as $name => $resource) {
      $build['resources'][$name] = [
        '#type' => 'details',
        '#title' => $this->t('@label', ['@label' => $resource->getLabel()]),
        '#open' => TRUE,
      ];

      foreach ($resource->getVariations() as $variation => $variation_info) {
        $build['resources'][$name][$variation] = [
          '#type' => 'details',
          '#title' => $this->t('@label', ['@label' => $variation_info['label']]),
          '#open' => TRUE,
        ];
        $build['resources'][$name][$variation]['properties'] = [
          '#type' => 'table',
          '#header' => [
            $this->t('Label'),
            $this->t('Name'),
            $this->t('Type'),
            $this->t('Operations'),
          ],
          '#tree' => TRUE,
          '#empty' => $this->t('There are no external entity properties.'),
        ];
        $operation_links = $this->buildOperationLinks($external_entity_type);

        foreach ($resource->getPropertiesByVariation($variation) as $property => $info) {
          $element = &$build['resources'][$name][$variation]['properties'][$property];
          $operation_key = "{$external_entity_type_id}.{$name}.{$variation}.{$property}";

          foreach (['label', 'name', 'type'] as $key) {
            if (!isset($info[$key])) {
              continue;
            }
            $element[$key]['#markup'] = $info[$key];
          }

          $element['operation'] = [
            '#type' => 'dropbutton',
            '#dropbutton_type' => 'small',
            '#links' => $operation_links[$operation_key]
              ?? $this->defaultOperationLinks(
                $property,
                $name,
                $variation,
                $external_entity_type_id
              )
          ];
        }
      }
    }

    if (count(Element::children($build)) === 0) {
      $this->messenger()->addError(
        $this->t('There are no external entity resources defined. Please check
          the @link.', [
            '@link' => $external_entity_type
              ->getStorageConnection()
              ->toLink('Resource Connection', 'collection')
              ->toString()
          ]
        )
      );
    }

    return $build;
  }

  /**
   * Default operation links.
   *
   * @param string $property
   *   The resource property name.
   * @param string $resource
   *   The resource name.
   * @param string $variation
   *   The resource variation.
   * @param string $external_entity_type_id
   *   The eternal entity type ID.
   *
   * @return array[]
   *   An array of the default operation links.
   */
  protected function defaultOperationLinks(
    string $property,
    string $resource,
    string $variation,
    string $external_entity_type_id
  ): array {
    return [
      'add' => [
        'title' => $this->t('Add Alteration'),
        'url' => Url::fromRoute('entity.external_entity_resource_alter.add_form', [
            'property' => $property,
            'resource' => $resource,
            'variation' => $variation,
            'external_entity_type' => $external_entity_type_id,
          ], ['attributes' => $this->ajaxModalAttributes()]
        ),
      ],
    ];
  }

  /**
   * Build external entity type resource operation links.
   *
   * @param \Drupal\external_entity\Contracts\ExternalEntityTypeInterface $external_entity_type
   *
   * @return array
   *    An array of operation links keyed by the alteration ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function buildOperationLinks(
    ExternalEntityTypeInterface $external_entity_type
  ): array {
    $links = [];

    foreach ($external_entity_type->getAllResourceAlterations() as $key => $alteration) {
      if ($alteration->hasLinkTemplate('edit-form')) {
        $links[$key]['edit'] = [
          'title' => $this->t('Edit Alteration'),
          'url' => $alteration->toUrl('edit-form', [
            'attributes' => $this->ajaxModalAttributes()
          ])
        ];
      }

      if ($alteration->hasLinkTemplate('delete-form')) {
        $links[$key]['delete'] = [
          'title' => $this->t('Delete Alteration'),
          'url' => $alteration->toUrl('delete-form', [
            'attributes' => $this->ajaxModalAttributes()
          ])
        ];
      }
    }

    return $links;
  }

  /**
   * Define the Ajax modal common attributes.
   *
   * @return array
   *   An array of the attributes.
   */
  protected function ajaxModalAttributes(): array {
    return [
      'class' => ['use-ajax'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode(['width' => 500]),
    ];
  }

}
