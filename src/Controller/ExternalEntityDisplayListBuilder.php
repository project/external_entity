<?php

declare(strict_types=1);

namespace Drupal\external_entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the external entity display list builder.
 */
class ExternalEntityDisplayListBuilder extends EntityListBuilder {

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    CurrentRouteMatch $current_route_match
  ) {
    parent::__construct($entity_type, $storage);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritDoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildHeader(): array {
    return [
      $this->t('Resource'),
      $this->t('Variation'),
      $this->t('Render Type'),
    ] + parent::buildHeader();
  }

  /**
   * @return string|null
   */
  protected function getExternalEntityTypeId(): ?string {
    return $this->currentRouteMatch->getParameter('external_entity_type');
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityIds(): array {
    $query = $this->getStorage()->getQuery();

    if ($external_entity_type = $this->getExternalEntityTypeId()) {
      $query->condition('external_entity_type_id', $external_entity_type)
        ->accessCheck();
    }
    $query->sort($this->entityType->getKey('id'));

    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\external_entity\Entity\ExternalEntityResourceDisplay $entity */
    return [
      $entity->getResource(),
      $entity->getVariation(),
      $entity->getRenderType()
    ] + parent::buildRow($entity);
  }

}
