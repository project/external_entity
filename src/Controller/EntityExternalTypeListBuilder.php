<?php

declare(strict_types=1);

namespace Drupal\external_entity\Controller;

use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the entity external type list builder.
 */
class EntityExternalTypeListBuilder extends EntityListBuilder {

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * External entity type list builder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    AccountProxyInterface $account,
    EntityStorageInterface $storage
  ) {
    parent::__construct($entity_type, $storage);
    $this->account = $account;
  }

  /**
   * {@inheritDoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    return new static(
      $entity_type,
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildHeader(): array {
    return [
      $this->t('Name'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\external_entity\Entity\ExternalEntityType $entity */
    return [
      $entity->label(),
    ] + parent::buildRow($entity);
  }

  /**
   * {@inheritDoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    /** @var \Drupal\external_entity\Entity\ExternalEntityType $operations */
    $operations = parent::getDefaultOperations($entity);

    if ($this->account->hasPermission('access all external entity type resources')) {
      $operations['resource'] = [
        'title' => $this->t('Manage resources'),
        'weight' => 10,
        'url' => Url::fromRoute('entity.external_entity_resource_alter.collection', [
          'external_entity_type' => $entity->id()
        ]),
      ];
    }

    if ($this->account->hasPermission('administer external entity resource displays')) {
      $operations['display'] = [
        'title' => $this->t('Manage displays'),
        'weight' => 15,
        'url' => Url::fromRoute('entity.external_entity_resource_display.collection', [
          'external_entity_type' => $entity->id()
        ]),
      ];
    }

    return $operations;
  }

}
