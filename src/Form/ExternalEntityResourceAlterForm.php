<?php

declare(strict_types=1);

namespace Drupal\external_entity\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entity\AjaxFormTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\Type\StringInterface;
use Drupal\Core\TypedData\Type\IntegerInterface;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the external entity resource alter form.
 */
class ExternalEntityResourceAlterForm extends ExternalEntityTypeAwareEntityForm {

  use AjaxFormTrait;

  /**
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   */
  public function __construct(FieldTypePluginManagerInterface  $field_type_manager) {
    $this->fieldTypeManager = $field_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(
    array $form,
    FormStateInterface $form_state
  ): array {
    /** @var \Drupal\external_entity\Entity\ExternalEntityResourceAlter $entity */
    $entity = $this->entity;

    if ($property_info = $entity->getResourcePropertyInfo()) {
      /** @var \Drupal\Core\Field\FieldTypePluginManager $field_type_manager */
      $field_type_manager = $this->fieldTypeManager;

      if (
        isset($property_info['type'])
        && $field_type_manager->hasDefinition($property_info['type'])
      ) {
        $type = $property_info['type'];
        $instance = $field_type_manager->createInstance($type, [
          'name' => NULL,
          'parent' => NULL,
          'field_definition' => BaseFieldDefinition::create($type)
        ]);

        $form['alterations'] = [
          '#type' => 'container',
          '#tree' => TRUE,
        ];
        $alterations = $entity->getAlterations();

        foreach ($instance->getProperties() as $name => $typed_data) {
          $form['alterations'][$name] = $this->buildFormElementFromTypedData($typed_data);
          $form['alterations'][$name] += [
            '#title' => $this->t('@label', [
              '@label' => $typed_data->getDataDefinition()->getLabel(),
            ]),
            '#default_value' => $alterations[$name] ?? NULL,
          ];
        }
      }
      else {
        $this->messenger()->addError(sprintf(
          'The %s property is not defined in the system.', $property_info['type']
        ));
      }
    }

    return parent::form($form, $form_state);
  }

  /**
   * Build form element from typed data.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $typed_data
   *   The typed data instance.
   *
   * @return array
   *   An array of form specific elements.
   */
  protected function buildFormElementFromTypedData(
    TypedDataInterface $typed_data
  ): array {
    if ($typed_data instanceof IntegerInterface) {
      return [
        '#type' => 'number',
      ];
    }

    if (
      $typed_data instanceof StringInterface
      && $typed_data instanceof OptionsProviderInterface
    ) {
      return [
        '#type' => 'select',
        '#options' => $typed_data->getPossibleOptions(),
      ];
    }

    return [
      '#type' => 'textfield',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = parent::save($form, $form_state);
    /** @var \Drupal\external_entity\Entity\ExternalEntityResourceAlter $entity */
    $entity = $this->entity;

    $form_state->setRedirectUrl(
      $entity->toUrl('collection')
    );

    return $status;
  }

  /**
   * {@inheritDoc}
   */
  protected function requiredRouteParameterInfo(): array {
    return [
      'property' => [
        'method' => 'setProperty'
      ],
      'resource' => [
        'method' => 'setResource'
      ],
      'variation' => [
        'method' => 'setVariation'
      ],
    ] + parent::requiredRouteParameterInfo();
  }

}
