<?php

declare(strict_types=1);

namespace Drupal\external_entity\Form;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\external_entity\Entity\ExternalEntityType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\BundleEntityFormBase;

/**
 * Define the external entity type default bundle form.
 */
class ExternalEntityTypeDefaultForm extends BundleEntityFormBase {

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\external_entity\Entity\ExternalEntityType $entity */
    $entity = $this->entity;

    $form['name'] = [
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#default_value' => $entity->label(),
      '#description' => $this->t(
        'The human-readable name for the external entity type.'
      ),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['type'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$entity->isNew(),
      '#machine_name' => [
        'exists' => [ExternalEntityType::class, 'load'],
        'source' => ['name'],
      ],
      '#description' => $this->t(
        'A unique machine-readable name for the external entity type.'
      ),
    ];

    $form['storage'] = [
      '#type' => 'details',
      '#title' => $this->t('Entity Storage'),
      '#description' => $this->t(
        'Configure how the external entity storage.'
      ),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['storage']['connection'] = [
      '#type' => 'select',
      '#title' => $this->t('Connection'),
      '#description' => $this->t('Select the external entity connection.'),
      '#options' => $this->getConnectionOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
      '#default_value' => $entity->getStorageConnectionId(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    /** @var \Drupal\external_entity\Entity\ExternalEntityType $entity */
    $entity = $this->entity;
    $status = $entity->save();

    $form_state->setRedirectUrl(
      $entity->toUrl('collection')
    );

    return $status;
  }

  /**
   * Get external entity connection options.
   *
   * @return array
   *   An array of connection options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getConnectionOptions(): array {
    $options = [];

    /** @var \Drupal\external_entity\Entity\ExternalEntityConnection[] $entities */
    $entities = $this->externalEntityConnectionStorage()->loadMultiple();

    foreach ($entities as $entity_id => $entity) {
      $options[$entity_id] = $entity->label();
    }

    return $options;
  }

  /**
   * External entity connection storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The eternal entity connection storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function externalEntityConnectionStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('external_entity_connection');
  }

}
