External Entity
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

The External Entity module was developed to bridge external entities that existed in various forms outside the Drupal ecosystem to be discoverable and used internally. The module provides a way to map external data properties with internal data models. The presentation of the external data will be displayed like any other internal entity, such as, node, paragraph, taxonomy term, etc.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/external_entity

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/external_entity

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

RECOMMENDED MODULES
-------------------

* [External Entity Server](https://www.drupal.org/project/external_entity_server):
  This module allows sharing entity data resources with other Drupal sites.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

More to come after we finalize/stabilize the codebase.

MAINTAINERS
-----------

Current maintainers:
* Travis Tomka (droath) - https://www.drupal.org/u/droath

This project has been sponsored by:

* Corning Museum of Glass (https://home.cmog.org)

